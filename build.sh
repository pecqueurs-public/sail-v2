#!/bin/bash

bold=$(tput bold)
normal=$(tput sgr0)

VERSION=$(awk '/^Version:/ { print $2 }' sail/DEBIAN/control)
echo "$VERSION" > ./sail/usr/lib/sail/version

dpkg-deb --build sail
FILENAME="$(awk '/^Package:/ { print $2 }' sail/DEBIAN/control)_${VERSION}_$(dpkg-architecture -qDEB_BUILD_ARCH).deb"

echo "Filename : $FILENAME"
mv ./sail.deb ./packages/$FILENAME
