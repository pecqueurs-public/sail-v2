#!/bin/bash

RED='\033[0;31m'
NC='\033[0m'

# Docker install
echo -e "${RED}Docker install${NC}"
echo "Which distibution is used ?"
distro=""
select yn in "debian" "ubuntu"; do
    case $yn in
        debian ) distro="debian"; break;;
        ubuntu ) distro="ubuntu"; break;;
    esac
done

sudo apt-get update
sudo apt-get -y install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    python2

sudo ln -s $(which python2) /usr/bin/python

curl -fsSL https://download.docker.com/linux/$distro/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg   
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/$distro \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io

sudo usermod -a -G docker $USER
printf "\nDOCKER_BUILDKIT=1;\n" >> $HOME/.bashrc


sudo docker swarm init

docker -v



# Docker-compose install 
echo -e "${RED}Docker-compose install${NC}"
installv2() {
    last_release=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep browser_download_url | grep docker-compose-$(uname -s | tr A-Z a-z)-$(uname -m | tr A-Z a-z) | cut -d '"' -f 4 | head -n 1)
    sudo curl -L $last_release -o /tmp/docker-compose
    sudo chmod +x /tmp/docker-compose

    if [ -d "/usr/local/lib/docker/cli-plugins" ]; then
        sudo mv /tmp/docker-compose /usr/local/lib/docker/cli-plugins/docker-compose
    elif [ -d "/usr/local/libexec/docker/cli-plugins" ]; then
        sudo mv /tmp/docker-compose /usr/local/libexec/docker/cli-plugins/docker-compose
    elif [ -d "/usr/lib/docker/cli-plugins" ]; then
        sudo mv /tmp/docker-compose /usr/lib/docker/cli-plugins/docker-compose
    elif [ -d "/usr/libexec/docker/cli-plugins" ]; then
        sudo mv /tmp/docker-compose /usr/libexec/docker/cli-plugins/docker-compose
    else
        mkdir -p $HOME/.docker/cli-plugins
        sudo mv /tmp/docker-compose $HOME/.docker/cli-plugins/docker-compose
        chown $USER: $HOME/.docker/cli-plugins/docker-compose
    fi

    curl -fL https://raw.githubusercontent.com/docker/compose-switch/master/install_on_linux.sh | sudo sh

    sudo ln -s $(which compose-switch) /usr/local/bin/docker-compose

    #update-alternatives --display docker-compose
}

# Install manual v2 (bug on v2 with "docker-compose -f $compose_file config" on deploy script => no version loaded)
sudo curl -L \
	"https://github.com/docker/compose/releases/download/2.27.1/docker-compose-$(uname -s)-$(uname -m)" \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

docker-compose -v



# Sailor
echo -e "${RED}Sailor install${NC}"
read -p "Which version sail do you want to use ?" sail_version

if [ -z "$sail_version" ]; then
    sail_version=2.0.0
fi

curl -L https://gitlab.com/api/v4/projects/36633509/repository/files/packages%2Fsail_${sail_version}_amd64.deb/raw?ref=master -o /tmp/sail.deb
sudo apt install /tmp/sail.deb

sail -v 

ln -s $HOME/.sail/stacks $HOME/projects
cp -R $HOME/.sail/ssh $HOME/projects/

echo "Launch Proxy ?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo -H -u ${USER} bash -c "sail proxy reload"; break;;
        No ) ;;
    esac
done

echo "Launch Portainer ?"
select yn in "Yes" "No"; do
    case $yn in
        Yes ) sudo -H -u ${USER} bash -c "sail portainer"; break;;
        No ) ;;
    esac
done


