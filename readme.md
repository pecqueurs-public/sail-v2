### Installation complete

```bash
curl -L https://gitlab.com/api/v4/projects/36633509/repository/files/install.sh/raw?ref=master -o /tmp/docker-install.sh
sudo chmod +x /tmp/docker-install.sh

/tmp/docker-install.sh
```

#### Troubleshooting

- Sur `WSL2` il est possible `docker` ne se lance pas :
```bash
sudo touch /etc/fstab

sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
sudo update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

sudo service docker start
``` 

- Si error : `dpkg-deb: error: archive '/tmp/sail.deb' uses unknown compression for member 'control.tar.zst'`
```bash
sudo apt-get install zstd binutils
sudo ar x /tmp/sail.deb
zstd -d < control.tar.zst | xz > control.tar.xz
zstd -d < data.tar.zst | xz > data.tar.xz
ar -m -c -a sdsd /tmp/sail_repacked.deb debian-binary control.tar.xz data.tar.xz
rm debian-binary control.tar.xz data.tar.xz control.tar.zst data.tar.zst

sudo apt install /tmp/sail_repacked.deb
```


### Installation de sail uniquement (ou MAJ)

```bash
sudo apt remove sail

sail_version=2.0.0

curl -L https://gitlab.com/api/v4/projects/36633509/repository/files/packages%2Fsail_${sail_version}_amd64.deb/raw?ref=master -o /tmp/sail.deb
sudo apt install /tmp/sail.deb
```

### Documentation

[WIKI](https://gitlab.com/pecqueurs-public/sail-v2/-/wikis/home)
