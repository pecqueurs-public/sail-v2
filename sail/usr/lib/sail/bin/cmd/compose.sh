#!/usr/bin/env bash

create_compose () {

    bash $SCRIPTS_PATH/docker_compose_run.sh ${1} "${@:2}"
}

remove_compose () {
    cd ~/projects/${1} && docker-compose down
}

