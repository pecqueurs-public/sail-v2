#!/usr/bin/env bash

container_exec () {
    local CONTAINER_NAME=${1}
    local OPTIONS=${@:2}

    bash $SCRIPTS_PATH/exec_in_container.sh $CONTAINER_NAME "$OPTIONS"
}

connect_to_container() {
    bash $SCRIPTS_PATH/prompt.sh "${@:1}"
}
