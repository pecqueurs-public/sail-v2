#!/usr/bin/env bash

proxy_show_config () {
    if [ ! -f $PROXY_PATH/nginx-proxy.conf ]; then
        echo "File 'nginx-proxy.conf' not found!"
        cp $SAILOR_PROXY_PATH/nginx-proxy.conf $PROXY_PATH/nginx-proxy.conf
    fi
    cat $PROXY_PATH/nginx-proxy.conf
}

proxy_edit_config () {
    vim $PROXY_PATH/server.ini
}

proxy_load () {
    $SAILOR_PROXY_PATH/load.sh
}

proxy_stop() {
    docker stack rm proxy_$USER
}

proxy_manager_init() {
    $SCRIPTS_PATH/init_proxy_manager.sh
}

proxy_manager_stop() {
    sail stack rm nginx-proxy-manager
}

portainer_init() {
    $SCRIPTS_PATH/init_portainer.sh
}
