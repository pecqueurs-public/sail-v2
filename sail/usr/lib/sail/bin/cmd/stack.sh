#!/usr/bin/env bash

create_stack () {
    bash $SCRIPTS_PATH/deploy.sh ${1} "${@:2}"
}

remove_stack () {
    docker stack rm ${1}
}

build_stack () {
    bash $SCRIPTS_PATH/build.sh ${1} "${@:2}"
}

pull_stack () {
    bash $SCRIPTS_PATH/pull_images.sh ${1} "${@:2}"
}

push_stack () {
    bash $SCRIPTS_PATH/push.sh ${1} "${@:2}"
}

reload_stack () {
    local SLEEP_TIME=${2:-60}

    docker stack rm ${1}
    for pc in $(seq 1 $SLEEP_TIME); do
        echo -ne "$pc/${SLEEP_TIME}s\033[0K\r"
        sleep 1
    done

    echo

    bash $SCRIPTS_PATH/deploy.sh ${1} "${@:2}"
}

scale_stack () {
    bash $SCRIPTS_PATH/scale_service.sh ${1} "${@:2}"
}

reload_service() {
    local MACHINE=${1}
    local STACK=${2}
    local SERVICE=${3}

    sail service scale $STACK $SERVICE 0
    sail service scale $STACK $SERVICE 1
}
