#!/usr/bin/env bash

case "$ACTION" in
    'stack')
        case "$TYPE" in
            'create')
                create_stack "${@:3}"
                ;;
            'rm')
                remove_stack $3
                ;;
            'reload')
                reload_stack $3 $4
                ;;
            'build')
                build_stack "${@:3}"
                ;;
            'pull')
                pull_stack "${@:3}"
                ;;
            'push')
                push_stack "${@:3}"
                ;;
            *)
                echo "Usage: $0 $ACTION [create|rm|reload|build|pull|push] [OPTIONS]"
                ;;    
        esac
        ;;
    'compose')
        case "$TYPE" in    
            'create')
                create_compose "${@:3}"
                ;;
            'rm')
                remove_compose $3
                ;;
            *)
                echo "Usage: $0 $ACTION [create|rm] [OPTIONS]"
                ;;    
        esac                   
        ;;

    'service')
        case "$TYPE" in 
            'reload')
                reload_service $3 $4
                ;;
            'scale')
                scale_stack "${@:3}"
                ;;
            *)
                echo "Usage: $0 $ACTION [reload|scale] [OPTIONS]"
                ;;        
        esac
        ;;            

    'proxy')
        PROXY_TYPE=$(getConfig PROXY_TO_USE)

        case "$TYPE" in
            'show')
                echo "proxy used: $PROXY_TYPE"
                if [ "$PROXY_TYPE" == "classic" ]; then
                    proxy_show_config
                else
                    echo "Command only executable for CLASSIC proxy."
                fi
                ;;
            'edit')
                echo "proxy used: $PROXY_TYPE"
                if [ "$PROXY_TYPE" == "classic" ]; then
                    proxy_edit_config
                else
                    echo "Command only executable for CLASSIC proxy."
                fi
                ;;
            'reload')
                echo "proxy used: $PROXY_TYPE"
                if [ "$PROXY_TYPE" == "classic" ]; then
                    proxy_load
                else
                    proxy_manager_init
                fi
                ;;
            'stop')
                echo "proxy used: $PROXY_TYPE"
                if [ "$PROXY_TYPE" == "classic" ]; then
                    proxy_stop
                else
                    proxy_manager_stop
                fi
                ;;
            #'api')
            #    proxy_api
            #    ;;
            *)
                echo "Usage: $0 $ACTION [show|edit|reload|stop]"
                ;;
        esac
        ;;

    'portainer')
        portainer_init
        ;;
    'container')
        case $TYPE in
            'exec')
                container_exec "$3" "${@:4}"
                ;;
            'connect')
                connect_to_container "${@:3}"
                ;;
            *)
                echo "Usage: $0 $ACTION [exec|connect] [OPTIONS]"
                ;;
        esac
        ;;

    'version')
        cat /usr/lib/sail/version
        cat /etc/sail/config.ini
        ;;

    'getConfig')
        echo $(getConfig $2)
        ;;

    'setConfig')
        setConfig $2 $3
        ;;



    ###### OLD COMMANDS ######

    'new')
        case "$TYPE" in
            'stack')
                echo "deprecated. Use 'sail stack create' instead."
                sail stack create "${@:1}"
                ;;
            'compose')
                echo "deprecated. Use 'sail compose create' instead."
                sail compose create "${@:1}"
                ;;
            *)
                echo "Usage: $0 $ACTION [stack|compose] [OPTIONS]"
                ;;
        esac
        ;;
    'rm')
        case "$TYPE" in
            'stack')
                echo "deprecated. Use 'sail stack rm' instead."
                sail stack rm "${@:1}" 
                ;;
            'compose')
                echo "deprecated. Use 'sail compose rm' instead."
                sail compose rm "${@:1}" 
                ;;
            *)
                echo "Usage: $0 $ACTION [stack|compose] [OPTIONS]"
                ;;
        esac
        ;;
    'reload')
        case "$TYPE" in
            'stack')
                echo "deprecated. Use 'sail stack reload' instead."
                sail stack reload "${@:1}" 
                ;;
            'service')
                echo "deprecated. Use 'sail service reload' instead."
                sail service reload "${@:1}"
                ;;
            *)
                echo "Usage: $0 $ACTION [stack] [OPTIONS]"
                ;;
        esac
        ;;
    'build')
        case "$TYPE" in
            'stack')
                echo "deprecated. Use 'sail stack build' instead."
                sail stack build "${@:1}"
                ;;
            *)
                echo "Usage: $0 $ACTION [stack] [OPTIONS]"
                ;;
        esac
        ;;
    'pull')
        case "$TYPE" in
            'stack')
                echo "deprecated. Use 'sail stack pull' instead."
                sail stack pull "${@:1}"
                ;;
            *)
                echo "Usage: $0 $ACTION [stack] [OPTIONS]"
                ;;
        esac
        ;;
    'scale')
        case "$TYPE" in
            'service')
                echo "deprecated. Use 'sail service scale' instead."
                sail service scale "${@:1}"
                ;;
            *)
                echo "Usage: $0 $ACTION [stack] [OPTIONS]"
                ;;
        esac
        ;;
        

    *)
        echo "Usage: $0 [stack|compose|service|proxy|container|version|getConfig|setConfig] [OPTIONS]"
        ;;
esac

