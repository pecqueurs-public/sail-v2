#!/usr/bin/env bash

LIB_PATH="/usr/lib/sail"

SCRIPTS_PATH="$LIB_PATH/bin/cmd"


# Create proxy folder if not exist
if [ ! -d "$HOME/.sail/proxy" ]; then
    mkdir -p $HOME/.sail/proxy
fi

# Create stacks folder if not exist
if [ ! -d "$HOME/.sail/stacks" ]; then
    mkdir -p $HOME/.sail/stacks
fi

# Create tls folder if not exist
if [ ! -d "$HOME/.sail/tls" ]; then
    mkdir -p $HOME/.sail/tls
fi
