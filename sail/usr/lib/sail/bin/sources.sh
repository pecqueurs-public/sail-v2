#!/usr/bin/env bash

LIB_PATH="/usr/lib/sail"

SOURCES_PATH="$LIB_PATH/bin/cmd"
SAILOR_PROXY_PATH="$LIB_PATH/lib/proxy"
SCRIPTS_PATH="$LIB_PATH/lib/tools"

DIRECTORY="$HOME/.sail"
PROXY_PATH="$DIRECTORY/proxy"

$LIB_PATH/bin/init.sh

source $SOURCES_PATH/exec.sh $LIB_PATH $PROXY_PATH
source $SOURCES_PATH/proxy.sh $LIB_PATH $PROXY_PATH
source $SOURCES_PATH/stack.sh $LIB_PATH $PROXY_PATH
source $SOURCES_PATH/compose.sh $LIB_PATH $PROXY_PATH
