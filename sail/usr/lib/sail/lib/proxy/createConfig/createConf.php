<?php

class CreateConf{
    const FILENAME_OUTPUT = 'nginx-proxy.conf';
    const DEFAULT_PORT = 80;
    const DEFAULT_PROXY_SSH_CONTENT="proxy.conf";

    const CERT_SSL_LE="cert_le.conf";
    const CERT_SSL_CUSTOM="cert_custom.conf";

    const PROXIED_CONTENT="proxied.conf";

    const AUTH_BASIC_CONF="auth_basic.conf";

    const INCLUDE_PATH_ALLOWED_IPS='/data/allowed_ips.conf';

    const TAG_BEGIN_AUTO = '#####  AUTO GENERATED CONFIG #####';
    const TAG_END_AUTO = '#####  AUTO GENERATED END #####';
    const TAG_BEGIN_CUSTOM = '#####  CUSTOM CONFIG #####';

    const LISTEN_PORT = 'LISTEN_PORT';
    const SERVER_NAME = 'SERVER_NAME';
    const ALLOW = 'ALLOW';
    const PROXY_SSH = 'PROXY_SSH';
    const PROXY_PASS = 'PROXY_PASS';
    const PROXIED = 'PROXIED';
    const OPTIONS = 'OPTIONS';
    const CERT_NAME = 'CERT_NAME';
    const AUTH_BASIC = 'AUTH_BASIC';

    private $config;
    private $template;
    private $autoGeneratedContentConf = '';
    private $customContentConf = '';
    private $rootPath;
    private $homePath;
    
    /**
     * CreateConf constructor.
     * @param string $file
     */
    public function __construct($file)
    {
        $this->rootPath = '/usr/lib/sail/lib/proxy/';
        $this->homePath = getenv('HOME') . '/.sail/proxy/';
        
        $this->config = parse_ini_file($this->homePath . $file, true);
        $this->template = file_get_contents($this->rootPath .'createConfig/template.conf');

        if (file_exists ( $this->homePath . self::FILENAME_OUTPUT)) {
            $content = file_get_contents($this->homePath . self::FILENAME_OUTPUT);
            $this->setCustomCode($content);
        }
    }

    public function parse()
    {
        foreach ($this->config as $server => $config) {
            $comment = $this->addComment($server);
            $this->autoGeneratedContentConf .= "\n" . $comment . $this->fill($config);
        }
    }

    public function save()
    {
        $content = self::TAG_BEGIN_AUTO
            . $this->autoGeneratedContentConf
            . self::TAG_END_AUTO . "\n\n"
            . self::TAG_BEGIN_CUSTOM . "\n\n"
            . $this->customContentConf;

        file_put_contents($this->homePath . self::FILENAME_OUTPUT, $content, LOCK_EX);
        echo 'Fichier créé : ' . $this->homePath . self::FILENAME_OUTPUT . "\n";
    }

    /**
     * @param string $content
     */
    private function setCustomCode($content)
    {
        $pattern = '/' . self::TAG_BEGIN_CUSTOM . '(.*)/is';
        if (preg_match($pattern, $content, $matches) && !empty($matches[1])) {
            $this->customContentConf = trim($matches[1]);
        }
    }

    /**
     * @param string $server
     * @return string
     */
    private function addComment($server)
    {
        return "# $server\n";
    }

    /**
     * @param array $server
     * @return string
     */
    private function fill(array $server)
    {
        if (empty($server['listen_port'])) {
            $server['listen_port'] = self::DEFAULT_PORT;
        }

        $server['proxied'] = empty($server['proxied']) || $server['proxied'] != 1
            ? ''
            : file_get_contents($this->rootPath .'createConfig/' . self::PROXIED_CONTENT);

        $server['proxy_ssh'] = !empty($server['proxy_ssh']) && $server['proxy_ssh'] == 0
            ? ''
            : file_get_contents($this->rootPath .'createConfig/' . self::DEFAULT_PROXY_SSH_CONTENT);

        $item = $this->template;

        // Check SSL
        if (!empty($server['cert_name'])) {

            $item .= "\nserver {
	listen {$server['listen_port']};
	listen [::]:{$server['listen_port']};
	server_name {$server['server_name']};
	return 301 https://\$host\$request_uri;
}\n\n";

            $certContent = !empty($server['cert_type']) && $server['cert_type'] === 'letencrypt'
                ? file_get_contents($this->rootPath .'createConfig/' . self::CERT_SSL_LE)
                : file_get_contents($this->rootPath .'createConfig/' . self::CERT_SSL_CUSTOM);
            
            $certificate = str_replace('[[CERT_NAME]]', $server['cert_name'], $certContent);
            $server['cert_name'] = $certificate;
            $server['listen_port'] = '443 ssl http2';

        } else {
            $server['cert_name'] = '';
        }

        if (!empty($server['auth_basic'])) {
            $passwd = file_get_contents($this->rootPath .'createConfig/' . self::AUTH_BASIC_CONF);
            $server['auth_basic'] = str_replace('[[AUTH_BASIC]]', $server['auth_basic'], $passwd);
        } else {
            $server['auth_basic'] = '';
        }

        return $this->replace($item, $server);
    }

    /**
     * @param string $item
     * @param array $config
     * @return string
     */
    private function replace($item, array $config)
    {
        $toReplace = [self::LISTEN_PORT, self::SERVER_NAME, self::ALLOW, self::PROXY_SSH, self::PROXY_PASS, self::PROXIED, self::OPTIONS, self::CERT_NAME, self::AUTH_BASIC];
        foreach ($toReplace as $key) {
            $configKey = strtolower($key);
            $value = !empty($config[$configKey]) || (isset($config[$configKey]) && $config[$configKey] == 0) ? $config[$configKey] : '';
            $item = $this->replaceOne($item, $key, $value);
        }

        return $item;
    }

    /**
     * @param string $template
     * @param string $key
     * @param string $value
     *
     * @return string
     */
    private function replaceOne($template, $key, $value)
    {
        switch ($key) {
            case self::ALLOW:
                return str_replace("[[$key]]", $this->createAllow($value), $template);
            case self::OPTIONS:
                return str_replace("[[$key]]", $this->createOptions($value), $template);
            default:
                return str_replace("[[$key]]", $value, $template);
        }
    }

    /**
     * @param string $value
     * @return string
     */
    private function createAllow($value)
    {
        $result = '';

        if (!empty($value)) {
            $exploded = explode(',', $value);
            foreach ($exploded as $allowIp) {
                $result .= "allow $allowIp;\n";
            }
        }

        if ($value !== 0 && $value !== '0') {
            $result .= "include " . self::INCLUDE_PATH_ALLOWED_IPS . ";\n";
        }

        return $result;
    }

    private function createOptions($value)
    {
        $result = '';
        if (is_array($value) && !empty($value)) {
            foreach ($value as $key => $option) {
                $result .= "$key $option;\n";
            }
        }

        return $result;
    }
}

$create = new CreateConf("server.ini");
$create->parse();
$create->save();