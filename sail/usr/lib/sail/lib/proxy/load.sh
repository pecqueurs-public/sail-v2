#!/usr/bin/env bash

DIRECTORY="$HOME/.sail/proxy"
PROXY_PATH="/usr/lib/sail/lib/proxy"
CONFIG_PATH="/etc/sail"

ORIGIN_PWD=$PWD

cd $DIRECTORY

echo $PWD

# Stop Nginx
echo "Stop proxy..."
#docker-compose down
docker stack rm proxy_$USER

# Create proxy common network
PROXY_NETWORK_ID=$(docker network ls -qf name=proxy_network)
if [ -z "$PROXY_NETWORK_ID" ]; then
  echo 'Create proxy common network...'
  docker network create --opt encrypted --driver overlay --attachable proxy_network
fi

# Recreate config
if [ -f $DIRECTORY/server.ini ]; then
    echo "Create config file..."
    /usr/bin/php $PROXY_PATH/createConfig/createConf.php
else
    echo "file 'server.ini' does not exist. No file generated."
fi

if [ ! -f $DIRECTORY/nginx-http.conf ]; then
    echo "File 'nginx-http.conf' not found!"
    cp $PROXY_PATH/nginx-http.conf $DIRECTORY/nginx-http.conf
fi

if [ ! -f $DIRECTORY/nginx-proxy.conf ]; then
    echo "File 'nginx-proxy.conf' not found!"
    cp $PROXY_PATH/nginx-proxy.conf $DIRECTORY/nginx-proxy.conf
fi

SWARM_STATE=$(docker info --format {{.Swarm.LocalNodeState}})

echo "$SWARM_STATE"

if [ ! -z "$SWARM_STATE" ] && [ "$SWARM_STATE" = 'inactive' ]; then
    echo "Active swarm mode ..."
    docker swarm init --advertise-addr=192.168.99.1
fi

#exit 0

# start Nginx
echo "Start proxy..."
#docker-compose up -d
docker stack deploy -c $CONFIG_PATH/proxy-stack.yml proxy_$USER

cd $ORIGIN_PWD