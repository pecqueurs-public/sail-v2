#!/usr/bin/env bash

# Define var
compose_build="docker-build.yml"
only=""
push=""
path_project="$HOME/projects/"
credentials_file="docker-credential.ini"

# Define host user id
export HOST_UID=$(ls -n ~/ | grep ' projects' | awk '{print $3}')
export HOST_GID=$(ls -n ~/ | grep ' projects' | awk '{print $4}')
export DOCKER_GID=$(sed -nr "s/^docker:x:([0-9]+):.*/\1/p" /etc/group)

# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0"  -o o:p:f:c:P --long "only:,path:,file:,credentials:,push"  -- "$@")

#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi

# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"


# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
    case "$1" in

        -o|--only)
            if [ -n "$2" ];
            then
                only="$only $2"
            fi
            shift 2;;

        -p|--path)
            if [ -n "$2" ];
            then
                path_project="$2"
            fi
            shift 2;;

        -f|--file)
            if [ -n "$2" ];
            then
                compose_build="$2"
            fi
            shift 2;;

        -c|--credentials)
            if [ -n "$2" ];
            then
                credentials_file="$2"
            fi
            shift 2;;

        -P|--push)
            push="push"
            shift ;;

        --)
            shift
            break;;
    esac
done

from=$PWD
cd $path_project

project_name=$1

# Search good path for project
if [ ! -d "$project_name" ]; then
    echo "Project seems not exist: $project_name" >&2
    return 1
fi

cd $project_name

# Build if needed
if [[ -f $compose_build ]]; then
    docker-compose -f $compose_build build $only

    if [ ! -z "$push" ] && [ "$push" = 'push' ]; then
        sail stack push "$project_name" -p "$path_project" -f "$compose_build" -c "$credentials_file" -o "$only"
    fi
fi

cd $from
