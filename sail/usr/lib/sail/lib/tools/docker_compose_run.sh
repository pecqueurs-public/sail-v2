#!/usr/bin/env bash

# Define var
compose_file="docker-compose.yml"
compose_build="docker-build.yml"
credentials_file="docker-credential.ini"
only=""
path_project="$HOME/projects/"

push=""
build=""
pull=""

# Define host user id
export HOST_UID=$(ls -n ~/ | grep ' projects' | awk '{print $3}')
export HOST_GID=$(ls -n ~/ | grep ' projects' | awk '{print $4}')
export DOCKER_GID=$(sed -nr "s/^docker:x:([0-9]+):.*/\1/p" /etc/group)


# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0"  -o o:p:f:F:c:PSB --long "only:,path:,file-stack:file-build:,credentials:,push,pull,build"  -- "$@")

#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi

# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"


# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
    case "$1" in

        -o|--only)
            if [ -n "$2" ];
            then
                only="$only $2"
            fi
            shift 2;;

        -p|--path)
            if [ -n "$2" ];
            then
                path_project="$2"
            fi
            shift 2;;

        -f|--file-stack)
            if [ -n "$2" ];
            then
                compose_file="$2"
            fi
            shift 2;;

        -F|--file-build)
            if [ -n "$2" ];
            then
                compose_build="$2"
            fi
            shift 2;;

        -c|--credentials)
            if [ -n "$2" ];
            then
                credentials_file="$2"
            fi
            shift 2;;

        -P|--push)
            push="-P"
            shift ;;

        -S|--pull)
            pull="pull"
            shift ;;

        -B|--build)
            build="build"
            shift ;;

        --)
            shift
            break;;
    esac
done

project_name=$1

from=$PWD
cd $path_project

# Search good path for project
if [ ! -d "$project_name" ]; then
    echo "Project seems not exist: $project_name" >&2
    return 1
fi

cd $project_name

# Search compose file
if [ ! -f $compose_file ]; then
    echo "Misses compose file: $compose_file" >&2
    return 1
fi

# Build if needed
if [ ! -z "$build" ] && [ "$build" = 'build' ] && [[ -f $compose_build ]]; then
    sail stack build "$project_name" $push -p "$path_project" -f "$compose_build" -c "$credentials_file" -o "$only"
fi

# Pull if needed
if [ ! -z "$pull" ] && [ "$pull" = 'pull' ] &&  [[ -f $compose_file ]]; then
    sail stack pull "$project_name" -p "$path_project" -f "$compose_build" -c "$credentials_file" -o "$only"
fi


# Deploy stack
STACK=$(echo "$(docker-compose -f $compose_file config 2>/dev/null)")

if [ -z "$STACK" ]; then
    echo "$(docker-compose -f $compose_file config 1>/dev/null)"
    return 1
fi

echo "$STACK" | docker-compose up -d

cd $from
