#!/bin.sh

CONTAINER_NAME=${1:=""}

CMD="${@:2}"

CONTAINER_ID=$(docker ps -qf name=$CONTAINER_NAME)

docker exec $CONTAINER_ID $CMD
