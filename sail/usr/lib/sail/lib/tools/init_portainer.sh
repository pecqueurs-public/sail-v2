#!/usr/bin/env bash

ORIGIN_PATH=$(pwd)
SAIL_PATH_CONFIG="/etc/sail"
PORTAINER_PATH_ORIGIN="/usr/lib/sail/lib/portainer"
PORTAINER_PATH="$HOME/projects/portainer"

if [ ! -d "$PORTAINER_PATH" ]; then
  echo "Create directory and init project 'portainer' in $PORTAINER_PATH ..."
  ln -s $HOME/.sail/stacks projects
  mkdir -p $PORTAINER_PATH
  cp -R $PORTAINER_PATH_ORIGIN $HOME/projects/

  # Create PROXY common network
  PROXY_NETWORK_ID=$(docker network ls -qf name=proxy_network)
  if [ -z "$PROXY_NETWORK_ID" ]; then
    echo 'Create proxy common network...'
    docker network create --opt encrypted --driver overlay --attachable proxy_network
  fi

  sed -i "s#ROOT_PATH=/home/debian/projects/portainer#ROOT_PATH=$PORTAINER_PATH#g" $PORTAINER_PATH/.env

  # Run PORTAINER
  sail stack create portainer
fi

cd $ORIGIN_PATH

