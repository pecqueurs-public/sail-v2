#!/usr/bin/env bash

ORIGIN_PATH=$(pwd)
SAIL_PATH_CONFIG="/etc/sail"
PROXY_PATH_ORIGIN="/usr/lib/sail/lib/nginx-proxy-manager"
PROXY_PATH="$HOME/projects/nginx-proxy-manager"

if [ ! -d "$PROXY_PATH" ]; then
  echo "Create directory and init project 'proxy manager' in $PROXY_PATH ..."
  ln -s $HOME/.sail/stacks projects
  mkdir -p $PROXY_PATH
  cp -R $PROXY_PATH_ORIGIN $HOME/projects/
fi


if [ ! -f "$PROXY_PATH/.env" ]
then
    echo "File .env does not exist... Init projects files."

    # Copy env file
    cp $PROXY_PATH/.env.example $PROXY_PATH/.env

    # Set password
    DB_PASSWORD=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)
    sed -i "s/password_to_change/$DB_PASSWORD/g" $PROXY_PATH/config.json
    sed -i "s/password_to_change/$DB_PASSWORD/g" $PROXY_PATH/.env

    cat $PROXY_PATH/config.json

    # Create proxy common network
    PROXY_NETWORK_ID=$(docker network ls -qf name=proxy_network)
    if [ -z "$PROXY_NETWORK_ID" ]; then
      echo 'Create proxy common network...'
      docker network create --opt encrypted --driver overlay --attachable proxy_network
    fi

    echo '---'

    cat $PROXY_PATH/.env



else
    echo "File .env exist... Reload project."

    # Stop proxy manager services
    sail stack rm nginx-proxy-manager

    sleep 10
fi

# Copy list of allowed ips
cp -rf $SAIL_PATH_CONFIG/allowed_ips.conf $PROXY_PATH/nginx_confd/include/allowed_ips.conf

# Run proxy manager
sail stack create nginx-proxy-manager

cd $ORIGIN_PATH

