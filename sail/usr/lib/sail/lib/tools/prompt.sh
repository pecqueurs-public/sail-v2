#!/usr/bin/env bash

DIR_TOOLS=$(dirname "$(readlink -f "$0")")
source "$DIR_TOOLS/consoleColor.sh"

getContainerName() {
    echo "$1" | awk -F "|" '{print $2}' | awk -F "." '{print $1}'
}

getContainerId() {
    local temp=$(echo "$1" | awk -F "|" '{print $1}')
    temp="${temp%\"}"
    echo "${temp#\"}"
}

ssh=''

abort() {
    echo -e "\e[ASelected : $(color -s bold -c red aborted)\n"
    exit 0
}

connect() {
    docker exec -it $container $shell
}

createArray() {
    local func="get$1"
    local -n currentArr="$2"

    func=${!func}
    mapfile -t currentArr <<<"$($func)"
}

displayArray() {
    local i=0
    local name
    local scope="$1"
    shift
    local arr=("$@")
    local len=${#arr[@]}

    [[ $scope == "containers" ]] && icon="\xF0\x9F\x93\xA6" || icon="\xF0\x9F\x8E\xAF"

    echo -e "\n$icon $(color -s bold $len $scope found !)\n"

    for elem in ${arr[@]}; do
        if (($i > 8)); then
            space=""
        fi

        [[ $scope == "containers" ]] && name=$(getContainerName $elem) || name=$elem
        echo -e "$space$(color -s dim $(($i + 1)))) $name"
        ((i++))
    done
}

list() {
    local -n selected="$2"
    local scope="$1"
    local space=" "
    local arr

    if [ ! -z $3 ]; then
        local -n localContainerName="$3"
    fi

    createArray $scope arr

    local len=${#arr[@]}

    if (($len == 1)); then
        local entry=${arr[0]}
        selected=$(getContainerId $entry)
        localContainerName=$(getContainerName $entry)
    else
        displayArray $scope ${arr[@]}

        while true; do
            echo -e "\nWich one ? $(color -s dim \(0 to abort\))"
            read -p "Selected : " id && [[ $id != 0 ]] || abort

            if (($id >= 1 && $id <= $len)); then
                local entry=${arr[$(($id - 1))]}
                local selectedEcho

                selected=$(getContainerId $entry)
                localContainerName=$(getContainerName $entry)

                selectedEcho=$(color -c green -s bold $localContainerName)

                echo -e "\e[ASelected : $selectedEcho"
                sleep 1
                break
            else
                echo "Please choose between 0 and $len"
            fi
        done
    fi
}

while [ -n "$1" ]; do # while loop starts
    case "$1" in
    -s)
        echo -e "\n\xF0\x9F\x96\xA5  Current shell : $(color -s bold -c magenta $2)"
        shell="$2"
        shift
        ;;
    --)
        shift # The double dash makes them parameters

        break
        ;;
    *)
        break
        ;;
    esac

    shift
done

# Paramas
[ ! -z $1 ] && container=$1 || container=
[ ! -z $shell ] && shell=$shell || shell="bash"


echoResultCmd=$""
getcontainers=$"docker ps -qf name=$container -f status=running --format \"{{.ID}}|{{.Names}}\""


list containers container containerName && connect

