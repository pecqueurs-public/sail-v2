#!/usr/bin/env bash

# Define var
compose_stacks="docker-stack.yml"
only=""
path_project="$HOME/projects/"
credentials_file="docker-credential.ini"

# Define host user id
export HOST_UID=$(ls -n ~/ | grep ' projects' | awk '{print $3}')
export HOST_GID=$(ls -n ~/ | grep ' projects' | awk '{print $4}')
export DOCKER_GID=$(sed -nr "s/^docker:x:([0-9]+):.*/\1/p" /etc/group)

# Execute getopt on the arguments passed to this program, identified by the special character $@
PARSED_OPTIONS=$(getopt -n "$0"  -o o:p:f:c: --long "only:,path:,file:,credentials:"  -- "$@")

#Bad arguments, something has gone wrong with the getopt command.
if [ $? -ne 0 ];
then
  exit 1
fi

# A little magic, necessary when using getopt.
eval set -- "$PARSED_OPTIONS"


# Now goes through all the options with a case and using shift to analyse 1 argument at a time.
#$1 identifies the first argument, and when we use shift we discard the first argument, so $2 becomes $1 and goes again through the case.
while true;
do
    case "$1" in

        -o|--only)
            if [ -n "$2" ];
            then
                only="$only $2"
            fi
            shift 2;;

        -p|--path)
            if [ -n "$2" ];
            then
                path_project="$2"
            fi
            shift 2;;

        -f|--file)
            if [ -n "$2" ];
            then
                compose_stacks="$2"
            fi
            shift 2;;

        -c|--credentials)
            if [ -n "$2" ];
            then
                credentials_file="$2"
            fi
            shift 2;;

        --)
            shift
            break;;
    esac
done

project_name=$1

from=$PWD
cd $path_project

# Search good path for project
if [ ! -d "$project_name" ]; then
    echo "Project seems not exist: $project_name" >&2
    return 1
fi

cd $project_name

# Pull if needed
if [[ -f $compose_stacks ]]; then
    if [ -f "$credentials_file" ]; then
        registry=$(awk -F "=" '/registry/ {print $2}' $credentials_file)
        user=$(awk -F "=" '/user/ {print $2}' $credentials_file)
        password=$(awk -F "=" '/password/ {print $2}' $credentials_file)

        docker login $registry -u $user -p $password
    fi

    docker-compose -f $compose_stacks pull $only
fi

cd $from
