#!/usr/bin/env bash
STACK=${1}
SERVICE_NAME=${2}
SCALE_VALUE=${3}

if [ -z "$SERVICE_NAME" ]; then
    echo "Service name not defined"
    exit 1
fi
if [ -z "$SCALE_VALUE" ]; then
    echo "Scale value not defined"
    exit 1
fi
echo "stack name: $STACK"
echo "service name: $SERVICE_NAME"
echo "scale to $SCALE_VALUE"
OLD_IFS=$IFS
IFS=$'\n'
ID=($(docker service ls -qf name=${STACK}_${SERVICE_NAME}))
IFS=$OLD_IFS

echo "${ID[@]}"

docker service scale ${ID[0]}=$SCALE_VALUE
